<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class IndexController extends Controller
{
    private const PAGINATE = 2;

    public function index(Request $request) {

        if($rating = $request->query('rating')) {
            if(in_array($rating, [10, 25, 50, 100])) {

                $posts = Post::with(['user'])->where('rating', '>=', $rating)->paginate(self::PAGINATE);
            }
        }

        if(!isset($posts)) {
            $posts = Post::with(['user'])->paginate(self::PAGINATE);
        }

        return response()->json([
            'posts' => $posts,
//            'ratingLimit' => ($ratingLimit ?? null),
//            'request' => $request->query()
        ]);
    }
}

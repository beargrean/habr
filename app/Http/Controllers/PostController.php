<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function getPost($postId)
    {
        $post = Post::with('user')->findOrFail($postId);

        return view('post', [
            'post' => $post,
        ]);
    }

//    public function getCompanyPost($companyNick, $postId)
//    {
//        $post = Post::with(['user', 'company'])->findOrFail($postId);
//
//        return view('company_post', [
//            'post' => $post,
//        ]);
//    }

//    public function updateUserPostRating($postId, Request $request)
//    {
//        return $this->updatePostRating($postId, $request);
//    }
//
//    public function updateCompanyPostRating($companyName, $postId, Request $request)
//    {
//        return response()->json([$companyName, $postId, $request]);
//        return $this->updatePostRating($postId, $request);
//    }

    public function updatePostRating(Request $request, $postId)
    {

        $post = Post::findOrFail($postId, ['id', 'votes', 'rating']);

        $votes = $post->votes;

        switch ($request->vote) {
            case 'plus':
                $votes->plusVotes += 1;
                break;
            case 'minus':
                $votes->minusVotes += 1;
                break;
            default:
                break;
        }

        $post->rating = $votes->plusVotes - $votes->minusVotes;

        $post->votes = $votes;

        $post->save();

        return response()->json($post->votes);
    }
}

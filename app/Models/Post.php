<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Posts
 *
 * @property int $id
 * @property int $userId
 * @property int|null $companyId
 * @property string $title
 * @property string $contentPreview
 * @property string $content
 * @property mixed $publicationDate
 * @property object|null $hubs
 * @property object|null $tags
 * @property object|null $marks
 * @property object $votes
 * @property int $rating
 * @property-read mixed $publicationDateFormatted
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereContentPreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereHubs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereMarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePublicationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereVotes($value)
 * @mixin \Eloquent
 * @property-read mixed $postUrl
 */
class Post extends Model
{
    public $timestamps = false;

    protected $casts = [
        'hubs' => 'object',
        'tags' => 'object',
        'marks' => 'object',
        'votes' => 'object',
        'publicationDate' => 'datetime:Y-m-d H:i:s'
    ];

    protected $dates = ['publicationDate'];

    public function getPublicationDateFormattedAttribute()
    {
        return Date::parse($this->publicationDate)->format('j F Y \\в h:i');
    }

    public function getPostUrlAttribute()
    {
        return route('user_post', ['post_id' => $this->id]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }

}

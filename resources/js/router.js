import VueRouter from 'vue-router';
import PostsList from './components/PostsList';
import PostPreview from "./components/PostPreview";

export default new VueRouter({
    routes: [
        {
            path: '/',
            component: PostsList
        },
        // {
        //     path: '/post',
        //     component: Post
        // }
    ],
    mode: 'history'
});
